#!/usr/bin/env python3

"""
description:    Reads a list of user ids and closes or locks the accounts
maintainer:     Brian Ó 🐟 <blacksam@gibberfish.org>
license:        MIT

If you find this script useful, please consider making a small tax-deductible
donation to Gibberfish, Inc. @ https://gibberfish.org/donate. Thank you.
"""

import argparse, diaspy, re

def diaspora_login(url, username, password):
    client = diaspy.connection.Connection(
        pod=url,
        username=username,
        password=password
    )
    client.login()
    return client

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--pod-url', help='The Diaspora* pod url')
    parser.add_argument('--user', help='Podmin user', default='podmin')
    parser.add_argument('--password', help='Podmin password', required=True)
    parser.add_argument('--file', help="Read user ids from a file",
                        default='fake-accounts.txt'
    )
    parser.add_argument('--verbose', help='Print results to stdout',
                        action='store_true', default=False)
    action = parser.add_mutually_exclusive_group(required=True)
    action.add_argument('--close', help='Close accounts',
                        action='store_true',
                        default=False
    )
    action.add_argument('--lock', help='Lock accounts',
                        action='store_true',
                        default=False
    )
    args = parser.parse_args()
    if re.match('https?://', args.pod_url):
        pod_url = args.pod_url
    else:
        pod_url = "https://%s" % args.pod_url
    client = diaspora_login(pod_url, args.user, args.password)
    with open(args.file, 'r') as file:
        for line in file.readlines():
            id = line.rstrip()
            if args.close:
                path = 'admin/users/%s/close_account' % id
            if args.lock:
                path = 'admin/users/%s/lock_account' % id
            r = client.post(path, data={},
                            headers={'X-CSRF-Token': client.get_token()})
            if args.verbose:
                print("%s: %s/%s" % (r.status_code, pod_url, path))

if __name__ == '__main__':
    main()
