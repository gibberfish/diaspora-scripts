#!/usr/bin/env python3
"""
description:    Prints a list of user accounts with zero-activity
maintainer:     Brian Ó 🐟 <blacksam@gibberfish.org>
license:        MIT

If you find this script useful, please consider making a small tax-deductible
donation to Gibberfish, Inc. @ https://gibberfish.org/donate. Thank you.
"""

import argparse

def get_users(args):
    """
    Return a list of database ids for users for which:
      - sign_in_count == 1
      - last_sign_in_at < interval days from present
      - there are zero posts, comments, likes, or private messages
      - no avatar has been uploaded
    """
    if args.mysql:
        import mysql.connector
        cnx = mysql.connector.connect(user=args.user, password=args.password,
            host=args.host, database=args.db)
        query = (
            "SELECT id,username FROM users WHERE sign_in_count = 1 "
            "AND last_sign_in_at < (CURDATE() - INTERVAL %s DAY)';" \
            % args.interval
        )
    else:
        import psycopg2
        cnx = psycopg2.connect(user=args.user, password=args.password,
            host=args.host, dbname=args.db)
        query = (
            "SELECT id,username FROM users WHERE sign_in_count = 1 "
            "AND last_sign_in_at < NOW() - interval '%s days';" % args.interval
        )
    cursor = cnx.cursor()
    cursor.execute(query)
    user_ids = []
    for (id,username) in cursor:
        person = get_person(cnx, id)
        if check_authorship(cnx, person, 'posts') or \
            check_authorship(cnx, person, 'comments') or \
            check_authorship(cnx, person, 'likes') or \
            check_authorship(cnx, person, 'messages') or \
            check_avatar(cnx, person):
            continue
        user_ids.append(id)
        if args.verbose:
            print("%s (%s)" % (username, id))
    cursor.close()
    cnx.close()
    return user_ids

def get_person(cnx, user_id):
    """
    Look up the associated 'person' id for the given 'user' id
    """
    query = ("SELECT id FROM people WHERE owner_id = %s")
    cursor = cnx.cursor()
    cursor.execute(query, (user_id,))
    (person_id,) = cursor.fetchone()
    cursor.close()
    return person_id

def check_avatar(cnx, person_id):
    """
    Return true if person has an avatar
    """
    query = (
        "SELECT FROM profiles WHERE person_id = %s "
        "AND image_url IS NOT NULL"
    )
    cursor = cnx.cursor()
    cursor.execute(query, (person_id,))
    return True if cursor.rowcount > 0 else False

def check_authorship(cnx, author_id, table):
    """
    Return true if user id is the author of a row in 'table'
    """
    query = ("SELECT FROM %s WHERE author_id = %s" % (table, author_id))
    cursor = cnx.cursor()
    cursor.execute(query)
    return True if cursor.rowcount > 0 else False

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--host', help='database server', default='127.0.0.1')
    parser.add_argument('--user', help='database user', default='diaspora')
    parser.add_argument('--password', help='database password', required=True)
    parser.add_argument('--db', help='database name',
                        default='diaspora_production')
    parser.add_argument('--interval',
                        help='ignore users created in the past n days',
                        default=30)
    parser.add_argument('--mysql', help='use mysql instead of postgres',
                        action='store_true', default=False)
    parser.add_argument('--verbose', help='Print list of usernames to stdout',
                        action='store_true', default=False)
    parser.add_argument('--file', help="Write user ids to a file",
                        default='fake-accounts.txt'
    )

    args = parser.parse_args()
    users = get_users(args)
    with open(args.file, 'w') as file:
        for id in sorted(users):
            file.write("%s\n" % id)

if __name__ == '__main__':
    main()
