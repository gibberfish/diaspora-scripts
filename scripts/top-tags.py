#!/usr/bin/python3
"""
description:    Prints a list of the most active hashtags from a diaspora pod
maintainer:     Brian Ó 🐟 <blacksam@gibberfish.org>
license:        MIT

If you find this script useful, please consider making a small tax-deductible
donation to Gibberfish, Inc. @ https://gibberfish.org/donate. Thank you.
"""

import argparse, re

def get_taglist(filename):
    """
    returns a list of hashtags from a file
    """
    taglist = []
    if filename is not None:
        with open(filename, 'r') as file:
            for line in file.readlines():
                for word in re.sub(',', ' ', line).split():
                    taglist.append(re.sub('^#', '', word))
    return taglist

def ignored_tags(list):
    """
    returns a list of hashtags as a series of 'WHERE' conditionals
    """
    query_part = ''
    for tag in list:
        query_part = query_part + " AND tags.name != '%s'" % tag
    return query_part

def do_query(args):
    """
    Runs the query using the selected database driver. Returns a list of tuples.
    """
    ignore_list = get_taglist(args.ignorefile)
    ignored = ignored_tags(ignore_list)
    if args.mysql:
        import mysql.connector
        cnx = mysql.connector.connect(user=args.user, password=args.password,
            host=args.host, database=args.db)
        query = ("SELECT tags.name, COUNT(taggings.tag_id) FROM taggings "
            "LEFT JOIN (tags, posts) ON (tags.id = taggings.tag_id AND "
            "posts.id = taggings.taggable_id) WHERE taggings.taggable_type = %s "
            "AND posts.public = 1 AND "
            "DATE_SUB(CURDATE(), INTERVAL %s DAY) <= taggings.created_at " +
            ignored + "GROUP BY taggings.tag_id "
            "ORDER BY COUNT(taggings.tag_id) DESC LIMIT %s;"
        )
    else:
        import psycopg2
        cnx = psycopg2.connect(user=args.user, password=args.password,
            host=args.host, dbname=args.db)
        query = ("SELECT tags.name, COUNT(taggings.tag_id) FROM taggings "
            "LEFT JOIN tags ON tags.id = taggings.tag_id LEFT JOIN posts ON "
            "posts.id = taggings.taggable_id WHERE taggings.taggable_type = %s "
            "AND posts.public = '1' AND "
            "taggings.created_at >= NOW() - '%s days'::INTERVAL " + ignored +
            "GROUP BY tags.name ORDER BY COUNT(taggings.tag_id) DESC LIMIT %s;"
        )
    cursor = cnx.cursor()
    cursor.execute(query, ('Post', args.days, int(args.count)))
    results = []
    for (tag, count) in cursor:
        if type(tag) is bytearray:
            tag = tag.decode('utf-8')
        results.append((tag, count))
    cursor.close()
    cnx.close()
    return results

def print_results(results, markdown=False):
    """
    Print formatted results
    """
    if markdown:
        print("| Tag | Count |")
        print("| --- | --- |")
    for r in results:
        if markdown:
            print("| #%s | %s |" % r)
        else:
            print("%s %s" % r)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--count', help="maximum number of results", default=10)
    parser.add_argument('--host', help='database server', default='127.0.0.1')
    parser.add_argument('--user', help='database user', default='diaspora')
    parser.add_argument('--password', help='database password', required=True)
    parser.add_argument('--db', help='database name',
                        default='diaspora_production')
    parser.add_argument('--mysql', help='use mysql instead of postgres',
                        action='store_true', default=False)
    parser.add_argument('--days', help='number of days', default=7)
    parser.add_argument('--ignorefile', help='file containing tags to ignore')
    parser.add_argument('--markdown', help='format output as a markdown table',
                        action='store_true', default=False)
    args = parser.parse_args()
    print_results(do_query(args), args.markdown)

if __name__ == '__main__':
    main()
